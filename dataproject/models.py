# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.

from django.db import models


class Deliveries(models.Model):
    match = models.ForeignKey('Matches', models.DO_NOTHING,
                              blank=True, null=True)
    inning = models.IntegerField(blank=True, null=True)
    batting_team = models.CharField(max_length=100, blank=True, null=True)
    bowling_team = models.CharField(max_length=100, blank=True, null=True)
    over = models.IntegerField(blank=True, null=True)
    ball = models.IntegerField(blank=True, null=True)
    batsman = models.CharField(max_length=100, blank=True, null=True)
    non_striker = models.CharField(max_length=50, blank=True, null=True)
    bowler = models.CharField(max_length=50, blank=True, null=True)
    is_super_over = models.IntegerField(blank=True, null=True)
    wide_runs = models.IntegerField(blank=True, null=True)
    bye_runs = models.IntegerField(blank=True, null=True)
    legbye_runs = models.IntegerField(blank=True, null=True)
    noball_runs = models.IntegerField(blank=True, null=True)
    penalty_runs = models.IntegerField(blank=True, null=True)
    batsman_runs = models.IntegerField(blank=True, null=True)
    extra_runs = models.IntegerField(blank=True, null=True)
    total_runs = models.IntegerField(blank=True, null=True)
    player_dismissed = models.CharField(max_length=50, blank=True, null=True)
    dismissal_kind = models.CharField(max_length=50, blank=True, null=True)
    fielder = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'deliveries'


class Matches(models.Model):
    id = models.IntegerField(primary_key=True)
    season = models.IntegerField(blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    match_date = models.DateField()
    team1 = models.CharField(max_length=100)
    team2 = models.CharField(max_length=100)
    toss_winner = models.CharField(max_length=100)
    toss_decision = models.CharField(max_length=50, blank=True, null=True)
    result = models.CharField(max_length=50, blank=True, null=True)
    dl_applied = models.IntegerField(blank=True, null=True)
    winner = models.CharField(max_length=100, blank=True, null=True)
    win_by_runs = models.IntegerField(blank=True, null=True)
    win_by_wicket = models.IntegerField(blank=True, null=True)
    player_of_match = models.CharField(max_length=50, blank=True, null=True)
    venue = models.CharField(max_length=100, blank=True, null=True)
    umpire1 = models.CharField(max_length=50, blank=True, null=True)
    umpire2 = models.CharField(max_length=50, blank=True, null=True)
    umpire3 = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'matches'
