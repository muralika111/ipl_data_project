from .models import Matches, Deliveries
from django.db.models import Count, Sum
from django.db.models.functions import Cast
from django.db.models import FloatField

# Create your views here.

from django.views.generic.list import ListView


class MatchesPlayedPerYear(ListView):
    model = Matches
    template_name = "dataproject/q1.html"

    def get_queryset(self):
        queryset = Matches.objects.values('season').annotate(
            no_of_matches=Count('season'))
        return queryset


class MatchesWonByAllTeamsOverAllTheYears(ListView):
    model = Matches
    template_name = "dataproject/q2.html"

    def get_queryset(self):
        queryset = Matches.objects.values(
            'winner', 'season').annotate(
                wins=Count('winner')).order_by('season')

        return queryset


class ExtraRunsConcededPerTeam(ListView):
    model = Deliveries
    template_name = "dataproject/q3.html"

    def get_queryset(self):
        queryset = Deliveries.objects.values('bowling_team').annotate(
            extra_runs=Sum('extra_runs')).filter(match__season=2016)

        return queryset


class TopEconomicalBowlers(ListView):
    model = Deliveries
    template_name = "dataproject/q4.html"

    def get_queryset(self):
        queryset = Deliveries.objects.values('bowler').annotate(
            economy=Cast(Sum(
                'total_runs')/(Count('ball')/6.0),
                FloatField())).filter(match__season=2015).order_by(
                    'economy')[:10]
        return queryset


class TopScorer(ListView):
    model = Deliveries
    template_name = "dataproject/q5.html"

    def queryset(self):
        queryset = Deliveries.objects.values('batsman').annotate(
            total_runs=Sum('total_runs')).order_by('-total_runs')[:10]
        return queryset
