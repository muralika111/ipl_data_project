import psycopg2

connection = psycopg2.connect(user="murali",
                              password="murali",
                              host="localhost",
                              port=5432,
                              database='ipl')

cursor = connection.cursor()
print("1. Plot the number of matches played per year of all the years in IPL.\n")
cursor.execute("select distinct season, count(season) as count from matches group by season;")
data = cursor.fetchall()
print(data)

print("------------------------------------------")

print("2. Plot a stacked bar chart of matches won of all teams over all the years of IPL.")
cursor.execute(
    "select season, winner, count(winner) as wins from matches group by winner, season order by season;")
data = cursor.fetchall()
print(data)
print("--------------------------------------------")

print("3. For the year 2016 plot the extra runs conceded per team.")
cursor.execute("select bowling_team, sum(extra_runs) as extra_runs from deliveries inner join matches on deliveries.match_id=matches.id where season=2016 group by bowling_team;")
data = cursor.fetchall()
print(data)
print("----------------------------------------------")


print("4. For the year 2015 plot the top economical bowlers.")
cursor.execute("select bowler, sum(total_runs)/(count(ball)/6) as economy from deliveries join matches on deliveries.match_id=matches.id where season=2015 group by bowler order by economy limit 10;")
data = cursor.fetchall()
print(data)

print("-----------------------------------------------")
